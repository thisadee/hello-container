#!/bin/env python3

from bottle import route, run, template
from os import environ

try:
  port=environ['PORT']
except:
  port=8080

# argparse
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--port', action="store", default="8080", dest="port")
args = parser.parse_args()

@route('<mypath:path>')
def test(mypath):
    return 'App1.0:Your path is: %s\n' % mypath

run(host='0.0.0.0', port=args.port, quiet=True)
