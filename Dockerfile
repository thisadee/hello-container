# hello world docker, mainly used to keep load balancers happy before the real app is deployed
# Based on: "joshuaconner/hello-world-docker-bottle"
FROM python:alpine
MAINTAINER Stein van Broekhoven <stein@aapjeisbaas.nl>

# patch it
RUN apk update && \
    apk upgrade --available

# install pip and hello server requirements
WORKDIR /app/
COPY server.py /app/server.py
RUN pip install bottle

# port config
EXPOSE 8080
ENTRYPOINT ["python", "/app/server.py", "--port", "8080"]
